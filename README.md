# Flappy Bird Clone

# License
I have chosen MIT license so as to allow for code contributions and free usage. 

# Deployment
To get the programming environment started you must ensure you have Visual Studio installed with Monogame.

# How to run the game
To play the built game follow the file path: AnthonyChangFinalProject\AnthonyChangFinalProject\bin\Windows\x86\Debug\
Double click on FlappyBirdClone.exe